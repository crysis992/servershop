package com.avalon.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.avalon.servershop.ServerShop;

public class Signs implements Listener {
	public static ServerShop plugin;
	public Signs(ServerShop instance)
	  {
	    plugin = instance;
	  } 
	
	
	
	@EventHandler(ignoreCancelled=true)
	public void onSignCreate(SignChangeEvent e) {
		Player p = e.getPlayer();
	if (e.getLine(0).equalsIgnoreCase("[ServerShop]") && !e.getLine(1).isEmpty()) {
		if (p.hasPermission("shop.sign.create")) {
			if (plugin.ShopSelect.containsKey(e.getLine(1))) {
			
		e.setLine(0, ChatColor.BLUE + "[Shop]");
		return;
			} else {
				p.sendMessage(ChatColor.RED + "There is no shop called " + e.getLine(1));
				e.setLine(0, ChatColor.RED + "[Invalid]");
				return;
			}
		
		} else {
			e.setLine(0, ChatColor.RED + "[Invalid]");
			p.sendMessage(ChatColor.RED + "You are not allowed to create ServerShop-GUI signs.");
			return;
		}
	}
	
	if (e.getLine(0).equals(ChatColor.BLUE + "[Shop]")) {
		e.setLine(0, ChatColor.RED + "[Invalid]");
		p.sendMessage(ChatColor.RED + "Wrong sign format");
		return;
	}
	}
	 
	@EventHandler(ignoreCancelled=true)
	public void onSignClick(PlayerInteractEvent e) {
	Player p = e.getPlayer();
	if (e.getAction()==Action.RIGHT_CLICK_BLOCK) {
		
	if (e.getClickedBlock().getType() == Material.WALL_SIGN || e.getClickedBlock().getType() == Material.SIGN_POST ) {
	Sign s = (Sign) e.getClickedBlock().getState();
	if (s.getLine(0).equals(ChatColor.BLUE + "[Shop]")) {
	if (plugin.ShopSelect.containsKey(s.getLine(1))) {
		int ID = plugin.ShopSelect.get(s.getLine(1));
		if (p.hasPermission("shop.open."+ s.getLine(1)) && p.hasPermission("shop.sign.use")) {
		p.openInventory(plugin.shops.get(ID));
		return;
	} else {
		p.sendMessage(ChatColor.RED + "You do not have permissions to open " + s.getLine(1));
		return;
	}
	} else {
		p.sendMessage(ChatColor.RED + "There is no shop called " + s.getLine(1));
		return;
	}
	}
	}
	}
	}
	
	
	@EventHandler(ignoreCancelled=true)
	public void onSignDestroy(BlockBreakEvent e) {
		if (e.getBlock().getType() == Material.WALL_SIGN || e.getBlock().getType() == Material.SIGN_POST ) {
			Sign s = (Sign) e.getBlock().getState();
			if (s.getLine(0).equals(ChatColor.BLUE + "[Shop]")) {
				if (!e.getPlayer().hasPermission("shop.sign.destroy")) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to break ServerShop Signs.");
				return;
				} else {
					e.getPlayer().sendMessage(ChatColor.GREEN + "ServerShop sign sucessfully removed.");
					return;
				}
			} 
		}
	}
}
