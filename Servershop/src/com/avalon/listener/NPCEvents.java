package com.avalon.listener;

import java.util.ArrayList;

import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.avalon.servershop.ServerShop;
import com.avalon.utils.Lang;

public class NPCEvents  implements Listener {


	public static ServerShop plugin;
	  private ArrayList<String> confirm = new ArrayList<String>();
	  public NPCEvents(ServerShop instance)
	  {
	    plugin = instance;
	  } 
	  
	  @EventHandler(ignoreCancelled=true)
	  public void onShopNPCclick(NPCRightClickEvent e) {
		  NPC selected = e.getNPC();
		  Player p = e.getClicker();
		  if (p.isSneaking() && p.hasPermission("shop.npc.remove")) {
			  if (plugin.getConfig().isSet("NPC." + selected.getId())) {
			  if (!confirm.contains(p.getName())) {
			  e.getClicker().sendMessage(Lang.NPC_REMOVE_CONFIRM.toString().replace("%npcname%", selected.getName()));
			  confirm.add(p.getName());
			  return;
			  }
			  else {
				  e.getClicker().sendMessage(Lang.NPC_REMOVE_SUCCESS.toString().replace("%npcname%", selected.getName()));
				  selected.destroy();
				  plugin.getConfig().set("NPC." + selected.getId(), null);
				  confirm.remove(p.getName());
				  return;
			  }
		  } else {
			  return;
		    }
		  }
		  	if (plugin.getConfig().isSet("NPC." + selected.getId())) {
			  if (p.hasPermission("shop.npc.interact")) {
				  if (p.hasPermission("shop.open." + plugin.getConfig().getString("NPC." + selected.getId()))) {
				  p.openInventory(plugin.shops.get(plugin.ShopSelect.get(plugin.getConfig().getString("NPC." + selected.getId()))));
				  return;
				  }
				  else {
					  p.sendMessage(Lang.NO_PERMISSION.toString());
				  }
		  } else {
			  p.sendMessage(Lang.NO_PERMISSION.toString());
			  return;
		  }
		  } else {
			  return;
		  }
	  }
	  }