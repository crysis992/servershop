package com.avalon.handler;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import com.avalon.utils.util;

public class SellHandler {

	
	private ItemStack i;
	private Player p;
	private String type;
	private String sell;
	private String PriceType;
	private String shopname;
	
	
	
	public SellHandler(ItemStack item, Player p, String shopname) {
		this.i = item;
		this.p = p;
		this.setType();
		this.setPrice();
		this.setPriceType();
		this.shopname = shopname;
	}
	
	private void setType() {
		
		String lore = this.i.getItemMeta().getLore().get(0);
		String[] loreargumente = lore.split(" ");
		this.type = loreargumente[1];
	}
	
	private void setPrice() {
		
		
		if (this.type.equals("Item")) {
		String lore = this.i.getItemMeta().getLore().get(2);
		String[] loreargumente = lore.split(" ");
		if (!ChatColor.stripColor(loreargumente[1]).equals("Unable")) {
		this.sell = ChatColor.stripColor(loreargumente[3]);
		} else {
			this.sell = ChatColor.stripColor(loreargumente[6]);
		}
		}
		
		
	}
	
	private void setPriceType() {
		
		if (this.type.equals("Item")) {
		
		String lore = this.i.getItemMeta().getLore().get(3);
		String[] loreargumente = lore.split(" ");
		this.PriceType = loreargumente[1];
		}
		
	}
	
	public boolean Sell() {
		
		if (this.type.equals("Item")) {
			
			ItemStack r = new ItemStack(i.getType(), i.getAmount(), (short) i.getDurability());
			ItemMeta meta = r.getItemMeta();
			if (i.hasItemMeta()) {
				if (i.getItemMeta().getDisplayName() != null) {
					meta.setDisplayName(i.getItemMeta().getDisplayName());
				}
				if (i.getItemMeta().getLore() != null) {
					
					List<String> lore = i.getItemMeta().getLore();
					List<String> remList = new ArrayList<String>();
					for(int i=0;i<=4;i++){
					remList.add(lore.get(i));
					}
					lore.removeAll(remList);	
					meta.setLore(lore);
				}
			}
			r.setItemMeta(meta);
			
			if (p.getInventory().containsAtLeast(r, i.getAmount())) {
				if (this.SellItem(this.PriceType, this.sell)) {
				p.getInventory().removeItem(r);
				util.logToFile(p.getName() + " bought " + this.i.getType() + " x" + this.i.getAmount() + " for " + this.sell + "[" + this.PriceType + "]", this.shopname + "-sold.txt");
				return true; 
				}
				} else {
					p.sendMessage(ChatColor.RED + "Not enough items of that type! You need " + i.getType() + " x" + i.getAmount() + ", or unable to sell.");
					return false; }
			}
			
			return false;
		}
	
	private boolean SellItem(String Pricetype, String amount) {
		
		if (amount.equals("Free")) {
			return true;
		}
		else if (amount.equals("Unable")) {
			return false;
		}
		
		switch(Pricetype) {
		case "Money":
			if (util.giveMoney(p, Float.parseFloat(this.sell))) {
				return true;
			} else { return false; }
		case "EXP":
			int lvl = new Double(this.sell).intValue();
			if (util.giveEXP(p, lvl)) {
				return true;
			} else { return false; }

		case "Item":
			int IPA = new Double(this.sell).intValue();
			if (util.giveItem(p, IPA)) {
				return true;
			} else { return false; }
		default:
			return false;
		}
	}
}
