package com.avalon.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.avalon.servershop.ServerShop;

public class util {
	
	  public static boolean LogsEnabled;	
	  public static Material pricetype;
	
	  
	    /**
	    * Check if the player has a free slot in his Inventory.
	    * @param player The Player object.
	    */  
	public static boolean hasSpot(Player player) {
		 int items = player.getInventory().firstEmpty();
		 if (items == -1) {
			 player.sendMessage(Lang.FULL_INVENTORY.toString());
			 player.closeInventory();
			 return false; 
		 }
		 return true;	 
	}
	
	
	/**
	 * Returns an ItemStack from a given String {@link itemString} example: WOOD:4 or 5:4
	 * Returns an Named Barrier ItemStack if the setup was incorrect.
	 * @param itemString
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static ItemStack StringToItemstack(String itemString) {
		
		String item = itemString.replace(':', ' ');
		String[] tmp = item.split(" ");
		
		Material mat = Material.BARRIER;
		int subID = 0;
		
		if (isInt(tmp[0])) {
			if (Material.getMaterial(Integer.parseInt(tmp[0])) != null) {
			mat = Material.getMaterial(Integer.parseInt(tmp[0]));
			}
		} else {
			if (Material.getMaterial(tmp[0].toUpperCase()) != null) {
			mat = Material.getMaterial(tmp[0].toUpperCase());
			}
		}

		if (tmp.length == 2 && isInt(tmp[1])) {
			subID = Integer.parseInt(tmp[1]);
		}
		
		ItemStack value = new ItemBuilder(mat).durability(subID).build();
		
		if (mat == Material.BARRIER && !itemString.contains("BARRIER")) {
			Bukkit.getLogger().info("=======================================================================================");
			Bukkit.getLogger().warning("[ERROR] Material Name NOT found for " + itemString.toUpperCase().replace(' ', '_'));
			Bukkit.getLogger().warning("[ERROR] Check the official bukkit API documentation for a full material list");
			Bukkit.getLogger().warning("[ERROR] http://jd.bukkit.org/beta/apidocs/org/bukkit/Material.html");
			Bukkit.getLogger().info("=======================================================================================");
			value = new ItemBuilder(value).name("�4ERROR").lore("Material name not found").lore("Please check your configuration!").build();
		}
		
		
		return value;
	}
	
	
	
		/**
	    * Take money from the player, returns false and send a message if the player has not enough money.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	@SuppressWarnings("deprecation")
	public static boolean payMoney(Player player, float money) {
		EconomyResponse r = ServerShop.econ.withdrawPlayer(player.getName(), money);
		if (r.transactionSuccess()) {
			player.sendMessage(Lang.PURCHASE_MONEY_COMPLETE.toString().replace("%amount%", ""+money).replace("%currencyname%", ServerShop.econ.currencyNameSingular()));
			return true;
		}
		else {
			player.sendMessage(Lang.NO_MONEY.toString());
			return false;
		}
	}
		/**
	    * Take Items from the player, returns false and send a message if the player has not enough Items.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean payitem(Player p, int money) {
		ItemStack i = new ItemStack(pricetype);
		if (p.getInventory().containsAtLeast(i, money)) {
		removeItem(p.getInventory(), pricetype, money);	
	    return true;
		} else {
			p.sendMessage(Lang.NO_ITEMS.toString().replace("%item%", pricetype.toString().toLowerCase().replace("_", " ")));
			return false;
		}
	}
	
	
		/**
	    * Give Items to the player, returns false and send a message if the player has not enough Inventory space.
	    * @param player The Player object.
	    * @param money the amount of money that should be taken.
	    */
	public static boolean giveItem(Player p, int money) {
		if (hasSpot(p)) {
			p.getInventory().addItem(new ItemStack(pricetype, money));
			p.sendMessage(ChatColor.GREEN + "Got " + money + " " + pricetype.name().toLowerCase());
			return true;
		} else {
			p.sendMessage(Lang.FULL_INVENTORY.toString());
			return false;
		}
	}
	
	public static boolean payEXP(Player p, int exp) {
		
		if (p.getLevel() >= exp) {
			
			p.setLevel(p.getLevel() - exp);
			p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5, 5);
			p.sendMessage(Lang.PURCHASE_LEVEL_COMPLETE.toString().replace("%level%", ""+exp));
			return true;
			
		} else {
			p.sendMessage(Lang.NO_LEVEL.toString());
			return false;
		}
	}

	
	
	
	@SuppressWarnings("deprecation")
	public static boolean giveMoney(Player player, float money) {
		EconomyResponse r = ServerShop.econ.depositPlayer(player.getName(), money);
		if (r.transactionSuccess()) {
			player.sendMessage(Lang.SELL_MONEY_COMPLETE.toString().replace("%amount%", ""+money).replace("%currencyname%", ServerShop.econ.currencyNameSingular()));
			return true;
		}
		else {
			player.sendMessage(ChatColor.RED + "Error while adding money to your Account");
			return false;
		}
	}
	
	
	
	public static boolean giveEXP(Player p, int exp) {
		int current = p.getLevel();
		p.setLevel(current + exp);
		p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 5, 1);
		p.sendMessage(Lang.SELL_LEVEL_COMPLETE.toString().replace("%level%", ""+ exp));
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static float calculateDiscount(float Betrag, float Discount, String type) {
		  float betragWert = Betrag;
		  float rabattWert = Discount;
		  
		  
		  float rabattFaktor = rabattWert / 100;
		  float betragDifferenz = betragWert * rabattFaktor;
		  float endBetrag = betragWert - betragDifferenz;

		  if (type.equalsIgnoreCase("endbetrag")) {
			  return endBetrag;
		  }
		  else if (type.equalsIgnoreCase("differenz")) {
			  return betragDifferenz;
		  }
		  else {
			  return Betrag;
		  }
	}
	
	 private static int removeItem(Inventory inventory, Material mat, int quantity) {
	        int rest = quantity;
	        for( int i = 0 ; i < inventory.getSize() ; i++ ){
	            ItemStack stack = inventory.getItem(i);
	            if( stack == null || stack.getType() != mat )
	                continue;
	            if( rest >= stack.getAmount() ){
	                rest -= stack.getAmount();
	                inventory.clear(i);
	            } else if( rest>0 ){
	                    stack.setAmount(stack.getAmount()-rest);
	                    rest = 0;
	            } else {
	                break;
	            }
	        }
	        return quantity-rest;
	    }
		public static void logToFile(String message, String file) {
			if (LogsEnabled) {
			
	        try {
	            File dataFolder = ServerShop.getInstance().getDataFolder();
	            File logFolder = new File(dataFolder.getAbsolutePath() + File.separator + "logs" + File.separator);
	            if(!dataFolder.exists())
	            {
	                dataFolder.mkdir();
	            }
	            if(!logFolder.exists())
	            {
	            	logFolder.mkdir();
	            } 
	            File saveTo = new File(dataFolder.getAbsolutePath() + File.separator + "logs" + File.separator +  file);
	            if (!saveTo.exists())
	            {
	                saveTo.createNewFile();
	            }
	            FileWriter fw = new FileWriter(saveTo, true);
	            PrintWriter pw = new PrintWriter(fw);
				Date dt = new Date();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				String time = df.format(dt);
	            pw.println("[" + time + "] " + message);
	            pw.close();
	 
	        } catch (IOException e)
	        {
	            e.printStackTrace();
	        }
			} else {
				return;
			}
	 
	    }
		
		public static boolean isInt(String amount) {
			try {
				Integer.parseInt(amount);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}
}
